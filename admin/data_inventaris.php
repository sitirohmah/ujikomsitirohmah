

	<?php
	include "header.php";
	?>
			 <div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-span6 ">
                    <div class="box-header" data-original-title>
                        <h2>Form Input Inventaris</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
                </div>
                    <div class="modal-body">
                       <form class="form-horizontal" action="simpan_inventaris.php" method="post" enctype="multipart/form-data">  
                              
                              <div class="control-group" >
                                <label class="control-label" >Nama </label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off" id="nama" name="nama" required="harus diisi" >
                                </div>
                              </div>

                               <div class="control-group" >
                                <label class="control-label" for="">Kondisi</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off" id="kondisi" name="kondisi" required="harus diisi" >
                                </div>
                              </div>

                              <div class="control-group" >
                                <label class="control-label" for="">Spesifikasi</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off"  name="spesifikasi" required="harus diisi" >
                                </div>
                              </div>

                               <div class="control-group" >
                                <label class="control-label" for="">Keterangan</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off" id="keterangan" name="keterangan" required="harus diisi" >
                                </div>
                              </div>

                               <div class="control-group " >
                                <label class="control-label" for="">Jumlah</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off" id="jumlah" name="jumlah" required="harus diisi" >
                                </div>
                              </div>

                               <div class="control-group">
                              <?php
                              include "../koneksi.php";
                              $result = mysqli_query($koneksi,"select * from jenis order by id_jenis asc ");
                              $jsArray = "var id_jenis = new Array();\n";
                               ?> 
                                <label class="control-label" for="">Id Jenis</label>
                                <div class="controls">
                                  <select class="span8 typeahead"  name="id_jenis" onchange="changeValue(this.value)">
                                  <option selected="selected">..........Pilih Jenis..........
  <?php 
  while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
    $jsArray .= "id_jenis['". $row['id_jenis']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
  }
  ?>
                             </option>
                             </select> 
                              </div>
                              </div>

                              <div class="control-group" >
                              <label class="control-label" for="">Tanggal Register</label>
                              <div class="controls">
                              <input type="date"  class="span8 typeahead" id="tanggal_register" name="tanggal_register" value="<?php $tgl = Date('Y-m-d'); echo $tgl ?>"  required="harus diisi" readonly >
                              </div>
                              </div>

                             <div class="control-group">
                             <?php
                             include "../koneksi.php";
                             $result = mysqli_query($koneksi,"select * from ruang order by id_ruang asc ");
                             $jsArray = "var id_ruang = new Array();\n";
                             ?> 
                             <label class="control-label" for="" >Id Ruang</label>
                             <div class="controls">
                             <select class="span8 typeahead"  name="id_ruang" onchange="changeValue(this.value)">
                             <option selected="selected">..........Pilih Ruang..........
       <?php 
       while($row = mysqli_fetch_array($result)){
       echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
       $jsArray .= "id_ruang['". $row['id_ruang']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
       }
       ?>                       
                            </option>
                            </select>                    
                            </div> 
                            </div>


                            <div class="control-group">
                            <label class="control-label" >Kode Inventaris</label>
                            <div class="controls">
                                <?php
                           $koneksi = mysqli_connect("localhost","root","","inventaris_smk");
                           
                           $cari_kd=mysqli_query($koneksi,"select max(kode_inventaris) as kode from inventaris");
                           //besar atau kode yang baru masuk
                           $tm_cari=mysqli_fetch_array($cari_kd);
                           $kode=substr($tm_cari['kode'],1,4);
                           $tambah=$kode+1;
                           if($tambah<10){
                            $kode_inventaris="V000".$tambah;
                           }else{ 
                            $kode_inventaris="V00".$tambah;
                           }
                           ?>
                            <input type="text" class="span8 typeahead" id="kode_inventaris" name="kode_inventaris" value="<?php echo $kode_inventaris; ?>" readonly
                            >                    
                            </div> 
                            </div>


                            <div class="control-group">
                             <?php
                             include "../koneksi.php";
                             $result = mysqli_query($koneksi,"select * from petugas order by id_petugas asc ");
                             $jsArray = "var id_petugas = new Array();\n";
                             ?> 
                            <label class="control-label" for="" >Id Petugas</label>
                            <div class="controls">
                            <select class="span8 typeahead" name="id_petugas" name="id_petugas" onchange="changeValue(this.value)">
                            <option selected="selected">..........Pilih ID Petugas..........
     <?php 
     while($row = mysqli_fetch_array($result)){
     echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
     $jsArray .= "id_petugas['". $row['id_petugas']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
     }
     ?>
  
                            </option>
                            </select>   
                            </div>
                            </div>      
                            <div class="control-group" >
                                <label class="control-label" for="">Sumber</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off" name="sumber" required="harus diisi" >
                                </div>
                              </div>             
                            
                           <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                            </form>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                      
               
<a href="#" data-toggle="modal" data-target="#myModal"><i class="btn icon-plus red"> Tambah Data </a></i>
        
<br>
<br>
		<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>Data Inventaris</h2>
						
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable table-responsive">
						  <thead>
                                        <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Kondisi</th>
                                        <th>Spesifikasi</th>
                                        <th>Keterangan</th>
                                        <th>Jumlah</th>
                                        <th>Nama Jenis</th>
                                        <th>Tanggal Register</th>
                                        <th>Nama Ruang</th>
                                        <th>Kode Inventaris</th>
                                        <th>Nama Petugas</th>
                                        <th>Sumber</th>
                                        <th>Aksi</th>
                                        </tr> 
                                    </thead>

                                 <tbody>
                                  
        
 <?php
include "../koneksi.php";
$no=1;
$bacadata = mysqli_query($koneksi,"select * from inventaris INNER JOIN jenis ON inventaris.id_jenis=jenis.id_jenis INNER JOIN ruang ON inventaris.id_ruang=ruang.id_ruang INNER JOIN petugas ON inventaris.id_petugas=petugas.id_petugas ORDER BY id_inventaris desc");
while($data = mysqli_fetch_array($bacadata))
{
  ?>

                                      <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nama']; ?></td>
                                        <td><?php echo $data['kondisi']; ?></td>
                                        <td><?php echo $data['spesifikasi']; ?></td>
                                        <td><?php echo $data['keterangan']; ?></td>
                                        <td><?php echo $data['jumlah']; ?></td>
                                        <td><?php echo $data['nama_jenis']; ?></td>
                                        <td><?php echo $data['tanggal_register']; ?></td>
                                        <td><?php echo $data['nama_ruang']; ?></td>
                                        <td><?php echo $data['kode_inventaris']; ?></td>
                                        <td><?php echo $data['nama_petugas']; ?></td>
                                        <td><?php echo $data['sumber']; ?></td>
                                       
                                        
                                            <td><a class="btn icon-edit red" href="edit_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>">Edit</a>  
                                            <a class="btn icon-trash blue" href="hapus_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>">Hapus</a></td> 


                                        </tr>

                                        <?php
                                      }
                                    ?>

                    
                                    </tbody>
                                </table>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>


		
	<?php
	include "footer.php";
	?>