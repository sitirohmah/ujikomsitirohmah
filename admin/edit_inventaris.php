<?php
include "../koneksi.php";
$id_inventaris = $_GET['id_inventaris'];

$select=mysqli_query($koneksi,"select * from inventaris where id_inventaris='$id_inventaris'");
$data=mysqli_fetch_array($select);
?>

<?php
include 'header.php';
?>
			<div class="row-fluid sortable">
				<div class="box span8">
					<div class="box-header" data-original-title>
						<h2>Form Edit Inventaris</h2>
						
					</div>
					<div class="box-content">
						<form class="form-horizontal" action="update_inventaris.php" method="post" role="form">
							 <input type="hidden" name="id_inventaris" value="<?php echo $data['id_inventaris'] ?>">
							 <input type="hidden" name="id_jenis" value="<?php echo $data['id_jenis'] ?>">
							 <input type="hidden" name="id_ruang" value="<?php echo $data['id_ruang'] ?>">
							 <input type="hidden" name="id_petugas" value="<?php echo $data['id_petugas'] ?>">
											  
							  <div class="control-group">
								<label class="control-label" for="disabledInput">ID Inventaris</label>
								<div class="controls">
								  <input class="span6 typeahead disabled" id="disabledInput" type="text" name="id_inventaris" disabled="" value="<?php echo $data['id_inventaris'];?>">
								</div>
							  </div>
							  
							  <div class="control-group" >
								<label class="control-label">Nama </label>
								<div class="controls">
								  <input type="text" class="span6 typeahead" name="nama" value="<?php echo $data['nama'];?>" >
								</div>
							  </div>	
							  <div class="control-group">
								<label class="control-label" >Kondisi</label>
								<div class="controls">
								  <input type="text" class="span6 typeahead" name="kondisi" value="<?php echo $data['kondisi'];?>">
								 
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label" >Spesifikasi</label>
								<div class="controls">
								  <input type="text" class="span6 typeahead" name="spesifikasi" value="<?php echo $data['spesifikasi'];?>">
								 
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label"  >Keterangan</label>
								<div class="controls">
								  <input type="text" class="span6 typeahead" name="keterangan" value="<?php echo $data['keterangan'];?>">
								 
								</div>
							  </div>

							  <div class="control-group">
								<label class="control-label" >Jumlah</label>
								<div class="controls">
								  <input type="number" class="span6 typeahead" name="jumlah" value="<?php echo $data['jumlah'];?>">
								 
								</div>
							  </div>
                              
                               <div class="control-group">
								<label class="control-label" for="disabledInput">ID Jenis</label>
								<div class="controls">
								  <input class="span6 typeahead disabled" id="disabledInput" type="text" name="id_jenis" disabled="" value="<?php echo $data['id_jenis'];?>">
								</div>
							  </div>

							  <div class="control-group">
								<label class="control-label">Tanggal Register</label>
								<div class="controls">
								  <input type="date" class="span6 typeahead" name="tanggal_register" value="<?php echo $data['tanggal_register'];?>">
								 
								</div>
							  </div>

							   <div class="control-group">
								<label class="control-label" for="disabledInput">ID Ruang</label>
								<div class="controls">
								  <input class="span6 typeahead disabled" id="disabledInput" type="text" name="id_ruang" disabled="" value="<?php echo $data['id_ruang'];?>">
								</div>
							  </div>

							  <div class="control-group">
								<label class="control-label" >Kode Inventaris</label>
								<div class="controls">
								  <input type="text" class="span6 typeahead" id="inputError" name="kode_inventaris" value="<?php echo $data['kode_inventaris'];?>" readonly>
								 
								</div>
							  </div>

							  

							   <div class="control-group">
								<label class="control-label" for="disabledInput">ID Petugas</label>
								<div class="controls">
								  <input class="span6 typeahead disabled" id="disabledInput" type="text" name="id_petugas" disabled="" value="<?php echo $data['id_petugas'];?>">
								</div>
							  </div>

							  <div class="control-group">
								<label class="control-label" >Sumber</label>
								<div class="controls">
								  <input type="text" class="span6 typeahead" name="sumber" value="<?php echo $data['sumber'];?>">
								 
								</div>
							  </div>

							   <div class="form-actions">
								<button type="submit" class="btn btn-primary">Simpan</button>
								<button type="reset" class="btn btn-primary">Reset</button>
	                           </div>
                               </form>
                               </div>
                               </div>
                               </div>
                               </div>
                               </div>
                               </div>

		
	<?php
	include 'footer.php';
	?>