<?php
include '../koneksi.php';
require('pdf/fpdf.php');

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('pdf/smkn1ciomas.jpg',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'INVENTARIS SMK',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'UJIKOM 2019',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'SMKN 1 CIOMAS',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Jalan Raya Laladon No. 2 RT 04/06 Desa Laladon Kec. Ciomas Kab. Bogor',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Barang",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama Barang', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Kondisi', 1, 0, 'C');
$pdf->Cell(2.5, 0.8, 'Keterangan', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Jumlah', 1, 0, 'C');
$pdf->Cell(2.5, 0.8, 'Jenis', 1, 0, 'C');
$pdf->Cell(2.5, 0.8, 'Tgl Register', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Ruang', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Kode Inventaris', 1, 0, 'C');
$pdf->Cell(3.5, 0.8, 'Petugas', 1, 0, 'C');
$pdf->Cell(3.5, 0.8, 'Sumber', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$query= mysqli_query($koneksi,"SELECT * from inventaris INNER JOIN jenis ON inventaris.id_jenis=jenis.id_jenis INNER JOIN ruang ON inventaris.id_ruang=ruang.id_ruang INNER JOIN petugas ON inventaris.id_petugas=petugas.id_petugas  order by id_inventaris desc ");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['nama'],1, 0, 'C');
	$pdf->Cell(2, 0.8, $lihat['kondisi'], 1, 0,'C');
	$pdf->Cell(2.5, 0.8, $lihat['keterangan'],1, 0, 'C');
	$pdf->Cell(2, 0.8, $lihat['jumlah'], 1, 0,'C');
	$pdf->Cell(2.5, 0.8, $lihat['nama_jenis'],1, 0, 'C');
	$pdf->Cell(2.5, 0.8, $lihat['tanggal_register'], 1, 0,'C');
	$pdf->Cell(2, 0.8, $lihat['nama_ruang'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['kode_inventaris'], 1, 0,'C');
	$pdf->Cell(3.5, 0.8, $lihat['nama_petugas'], 1, 0,'C');
	$pdf->Cell(3.5, 0.8, $lihat['sumber'], 1, 1,'C');





	$no++;
}

$pdf->Output("cetak_barang.pdf","I");

?>

