<?php
include 'header.php';
?>
			
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-span6 ">
                    <div class="box-header" data-original-title>
                        <h2>Form Input Ruang</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
                </div>
                    <div class="modal-body">
                        <form class="form-horizontal" action="simpan_ruang.php" method="post" enctype="multipart/form-data">  
                              
                              <div class="control-group" >
                                <label class="control-label" >Nama Ruang :</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off" id="nama_ruang" name="nama_ruang" required="harus diisi" >
                                </div>
                              </div>

                               <div class="control-group" >
                                <label class="control-label" for="">Kode Ruang :</label>
                                <div class="controls">
                                    <?php
                                    include "../koneksi.php";
                           $koneksi = mysqli_connect("localhost","root","","inventaris_smk");
                           
                           $cari_kd=mysqli_query($koneksi,"select max(kode_ruang) as kode from ruang");
                           //besar atau kode yang baru masuk
                           $tm_cari=mysqli_fetch_array($cari_kd);
                           $kode=substr($tm_cari['kode'],1,4);
                           $tambah=$kode+1;
                           if($tambah<10){
                            $kode_ruang="R000".$tambah;
                           }else{ 
                            $kode_ruang="R00".$tambah;
                           }
                           ?>
                                  <input type="text" class="span8 typeahead" autocomplete="off" id="kode_ruang" name="kode_ruang" required="harus diisi"  value="<?php echo $kode_ruang; ?>" readonly >
                                </div>
                              </div>

                              <div class="control-group" >
                                <label class="control-label" for="">Keterangan :</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off"  name="keterangan" required="harus diisi" >
                                </div>
                              </div>

                              
                            
                            <br>

                            <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                            </form>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                           
    
<a href="#" data-toggle="modal" data-target="#myModal"><i class="btn icon-plus red"> Tambah Data </a></i>
<br>
<br>
		<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>Data Ruang</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
                                        <tr>
                                        <th>No</th>
                                        <th>Nama Ruang</th>
                                        <th>Kode Ruang</th>
                                        <th>Keterangan</th>
                                        <th>Aksi</th>
                                        </tr> 
                                    </thead>

                                 <tbody>
                                  
        
 <?php
include "../koneksi.php";
$no=1;
$bacadata = mysqli_query($koneksi,"select * from ruang order by id_ruang desc");
while($data = mysqli_fetch_array($bacadata))
{
  ?>

                                      <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nama_ruang']; ?></td>
                                        <td><?php echo $data['kode_ruang']; ?></td>
                                        <td><?php echo $data['keterangan']; ?></td>
                       
                                            <td><a class="btn icon-edit red" href="edit_ruang.php?id_ruang=<?php echo $data['id_ruang']; ?>">Edit</a>  
                                            <a class="btn icon-trash blue" href="hapus_ruang.php?id_ruang=<?php echo $data['id_ruang']; ?>">Hapus</a></td> 


                                        </tr>

                                        <?php
                                      }
                                    ?>

                    
                                    </tbody>
                                </table>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>


		
<?php
include 'footer.php';
?>