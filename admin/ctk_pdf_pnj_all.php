<?php
include '../koneksi.php';
require('pdf/fpdf.php');

$pdf = new FPDF("L","cm","A4");
$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('pdf/smkn1ciomas.jpg',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'INVENTARIS SMK',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'UJIKOM 2019',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'SMKN 1 CIOMAS',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Jalan Raya Laladon No. 2 RT 04/06 Desa Laladon Kec. Ciomas Kab. Bogor',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Peminjaman",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Kode Pinjam', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama Barang', 1, 0, 'C');
$pdf->Cell(3.5, 0.8, 'Jumlah Pinjam', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Tanggal Pinjam', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Tanggal Kembali', 1, 0, 'C');
$pdf->Cell(3.5, 0.8, 'Nama Pegawai', 1, 0, 'C');
$pdf->Cell(3.5, 0.8, 'Status Peminjaman', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;





$data = mysqli_query($koneksi,"SELECT * from peminjaman i left join detail_pinjam p on p.id_detail_pinjam=i.id_peminjaman left join inventaris v on p.id_inventaris=v.id_inventaris left join pegawai c on c.id_pegawai=i.id_pegawai order by id_peminjaman desc ");

while($lihat=mysqli_fetch_array($data)){
$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
$pdf->Cell(3, 0.8, $lihat['kode_pinjam'], 1, 0, 'C');
$pdf->Cell(3, 0.8, $lihat['nama'], 1, 0, 'C');
$pdf->Cell(3.5, 0.8, $lihat['jumlah_pinjam'], 1, 0, 'C');
$pdf->Cell(3, 0.8, $lihat['tanggal_pinjam'], 1, 0, 'C');
$pdf->Cell(3, 0.8, $lihat['tanggal_kembali'], 1, 0, 'C');
$pdf->Cell(3.5, 0.8, $lihat['nama_pegawai'], 1, 0, 'C');
$pdf->Cell(3.5, 0.8, $lihat['status_peminjaman'], 1, 1, 'C');
  
  
  $no++;
}

$pdf->Output("cetak_peminjaman.pdf","I");

?>

