<!DOCTYPE html>
<html>
<head>
  <title>Inventaris SMK</title>
</head>
<body>
  <style type="text/css">
    body{
      font-family: sans-serif;
    }
    table{
      margin: 20px auto;
      border-collapse: collapse;
    }
    table th,
    table td{
      border: 1px solid #3c3c3c;
      padding: 3px 8px;

    }
    a{
      background: blue;
      color: #fff;
      padding: 8px 10px;
      text-decoration: none;
      border-radius: 2px;
    }
  </style>

  <?php
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Data Barang.xls");
  ?>

  <center>
    <h1>Data Barang </h1>
  </center>

  <table border="1">
   <thead>
    <tr>
     <th>No</th>
     <th>Nama</th>
     <th>Kondisi</th>
     <th>Keterangan</th>
     <th>Jumlah</th>
     <th>Id Jenis</th>
     <th>Tanggal Registrasi</th>
     <th>Id Ruang</th>
     <th>Kode Inventaris</th>
     <th>Id Petugas</th>
     <th>Sumber</th>
   </tr>
 </tr>
</thead>
<tbody>
  <?php
  include '../koneksi.php';
  $no =1;
  $tanggal_awal = $_POST['tgl_a'];
  $tanggal_akhir = $_POST['tgl_b'];
  $data = mysqli_query($koneksi,"SELECT * FROM inventaris INNER JOIN jenis ON inventaris.id_jenis=jenis.id_jenis  INNER JOIN ruang ON inventaris.id_ruang=ruang.id_ruang INNER JOIN petugas ON inventaris.id_petugas=petugas.id_petugas  where tanggal_register between '$tanggal_awal' and '$tanggal_akhir' ");
  while($r = mysqli_fetch_array($data)){
    ?>

    <tr>
      <td><?php echo $no++;?></td>
      <td><?php echo $r['nama']; ?></td>
      <td><?php echo $r['kondisi']; ?></td>
      <td><?php echo $r['keterangan']; ?></td>
      <td><?php echo $r['jumlah']; ?></td>
      <td><?php echo $r['nama_jenis']; ?></td>
      <td><?php echo $r['tanggal_register']; ?></td>
      <td><?php echo $r['nama_ruang']; ?></td>
      <td><?php echo $r['kode_inventaris']; ?></td>
      <td><?php echo $r['nama_petugas']; ?></td>
      <td><?php echo $r['sumber']; ?></td>






    </tr>
    <?php
  }
  ?>
</tbody>
</table>

</body>
</html>