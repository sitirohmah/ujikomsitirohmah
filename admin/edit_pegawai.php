<?php
include "../koneksi.php";
$id_pegawai = $_GET['id_pegawai'];

$select=mysqli_query($koneksi,"select * from pegawai where id_pegawai='$id_pegawai'");
$data=mysqli_fetch_array($select);
?>

<?php
include 'header.php';
?>


			<div class="row-fluid sortable">
				<div class="box span8">
					<div class="box-header" data-original-title>
						<h2>Form Edit Pegawai</h2>
						
					</div>
					<div class="box-content">
						<form class="form-horizontal" action="update_pegawai.php" method="post" role="form">
							 <input type="hidden" name="id_pegawai" value="<?php echo $data['id_pegawai'] ?>">
							
											  
							  <div class="control-group">
								<label class="control-label" for="disabledInput">ID Pegawai</label>
								<div class="controls">
								  <input class="span6 typeahead disabled" id="disabledInput" type="text" name="id_pegawai" disabled="" value="<?php echo $data['id_pegawai'];?>">
								</div>
							  </div>

							   <div class="control-group" >
								<label class="control-label">Nama Pegawai </label>
								<div class="controls">
								  <input type="text" class="span6 typeahead" name="nama_pegawai" value="<?php echo $data['nama_pegawai'];?>" >
								</div>
							  </div>	
							  <div class="control-group">
								<label class="control-label" >NIP</label>
								<div class="controls">
								  <input type="text" class="span6 typeahead" name="nip" value="<?php echo $data['nip'];?>">
								 
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label"  >Alamat</label>
								<div class="controls">
								  <input type="text" class="span6 typeahead" name="alamat" value="<?php echo $data['alamat'];?>">
								 
								</div>
							  </div>

							  
							   <div class="form-actions">
								<button type="submit" class="btn btn-primary">Simpan</button>
								<button type="reset" class="btn btn-danger">Reset</button>
	                           </div>
                               </form>
                               </div>
                               </div>
                               </div>
                               </div>
                               </div>
                               </div>

		
	<?php
include 'footer.php';
?>