DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(11) NOT NULL,
  `status` enum('Y','N','','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("1","2","1","2","N");
INSERT INTO detail_pinjam VALUES("19","1","1","1","N");
INSERT INTO detail_pinjam VALUES("20","2","1","2","N");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `spesifikasi` text NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(20) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `sumber` text NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("1","Laptop","baik","Intel ","ada di Lab 2","35","1","2019-02-12","1","V0001","1","PT Maju Prima");
INSERT INTO inventaris VALUES("2","Kursi","baik","warna coklat","aaaaa","37","2","2019-02-20","2","V0002","2","dari PT");
INSERT INTO inventaris VALUES("3","Meja","baik","warna coklat","ada digudang","39","1","2019-02-27","3","V0003","3","Pt.Suka Maju");
INSERT INTO inventaris VALUES("4","Proyektor","Baik","abcde","abcd","39","1","2019-04-04","1","V0004","1","Amir");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(30) NOT NULL,
  `kode_jenis` varchar(30) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("1","Elektronik","J0001","Di Lab 1");
INSERT INTO jenis VALUES("2","Non Elektronik","J0002","Di Gudang");
INSERT INTO jenis VALUES("6","Mebel","J0003","Di Lantai Bawah");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(30) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");
INSERT INTO level VALUES("2","operator");
INSERT INTO level VALUES("3","peminjam");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(30) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `alamat` varchar(40) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("2","lala","1213dfe","bogor");
INSERT INTO pegawai VALUES("3","Fahmi","000123","davdav");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `kode_pinjam` varchar(30) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` enum('Pinjam','Kembali','','') NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_pegawai` (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("1","PNJ0001","2019-04-06","2019-04-06","Kembali","2");
INSERT INTO peminjaman VALUES("2","PNJ0002","2019-04-06","2019-04-06","Kembali","2");
INSERT INTO peminjaman VALUES("4","PNJ0003","2019-04-06","0000-00-00","","0");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(30) NOT NULL,
  `nama_petugas` varchar(30) NOT NULL,
  `id_level` int(11) NOT NULL,
  `banned` enum('N','Y','','') NOT NULL,
  `logintime` int(11) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","operator","","operator4321","rahma","2","N","0");
INSERT INTO petugas VALUES("2","admin","strhmh811@gmail.com","admin4321","siti rahma","1","N","0");
INSERT INTO petugas VALUES("3","peminjam","","peminjam4321","Danu Ilham","3","N","0");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(30) NOT NULL,
  `kode_ruang` varchar(30) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Di Lab 1","R0001","Baik");
INSERT INTO ruang VALUES("2","Di Lab 2","R0002","Baik");
INSERT INTO ruang VALUES("3","Di Gudang","R0003","Baik");



DROP TABLE smtr_detail;

CREATE TABLE `smtr_detail` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




