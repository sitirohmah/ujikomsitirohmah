
<?php 
include "header.php";
?>

    <button type="button"  data-toggle="modal" data-target="#export" class="btn icon-print red"><i class="fa fa-print" aria-hidden="true" style="color: white;"> Cetak Ke PDF</i></a></button>
                                    <button type="button" data-toggle="modal" data-target="#excel" class="btn icon-share-alt green" aria-hidden="true" style="color: white;"> Cetak Ke Excel</i></a></button>
                                    <br><br>
                      <div class="row-fluid sortable">      
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2>Data Laporan Peminjaman</h2>
                       
                    </div>
                    <div class="box-content">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode Pinjam</th>
                                            <th>Nama Barang</th>
                                            <th>Jumlah Pinjam</th>
                                             <th>Tanggal Pinjam</th>
                                             <th>Tanggal Kembali</th>
                                             <th>Nama Pegawai</th>
                                            <th>Status Peminjaman</th>
                                              
                                            
                                            
                                        </tr>
                                        </thead>


                                        <tbody>
                                         <?php
                                        include '../koneksi.php';
                                        $no =1;
                                        $data = mysqli_query($koneksi,"select * from peminjaman i left join detail_pinjam p on p.id_detail_pinjam=i.id_peminjaman 
                      left join inventaris v on p.id_inventaris=v.id_inventaris left join pegawai c on c.id_pegawai=i.id_pegawai 
                        order by id_peminjaman desc ");
                                        while($r = mysqli_fetch_array($data)){
                                        ?>
                                        <tr>
                                            <td><?php echo $no++;?></td>
                                            <td><?php echo $r['kode_pinjam']; ?></td>
                                            <td><?php echo $r['nama']; ?></td>
                                            <td><?php echo $r['jumlah_pinjam']; ?></td>
                                            <td><?php echo $r['tanggal_pinjam']; ?></td>
                                             <td><?php echo $r['tanggal_kembali']; ?></td>
                                             <td><?php echo $r['nama_pegawai']; ?></td>
                                            <td><span class="label label-success"><?php echo $r['status_peminjaman']; ?></td>
                                           
                                          
                                           
                                           
                                            

                                        </tr>
                                       <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                   
                                    
                                </div>
                            </div>
                        </div>



<div id="export" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Data Peminjaman</h4>
</div>
<div class="modal-body">
<form action="ctk_pdf_pnj.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Cetak">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="ctk_pdf_pnj_all.php" target="_blank" class="btn btn-primary" >Cetak Semua</a>
</div>
</div>
</div>
</div>
</div>



<div id="excel" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Data Peminjaman </h4>
</div>
<div class="modal-body">
<form action="ctk_excel_pnj.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-danger" value="Cetak">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="ctk_all_excel_pnj.php" target="_blank" class="btn btn-info btn-sm" >Cetak Semua</a>
</div>
</div>
</div>
</div>
</div>
</div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
           <?php
           include "footer.php";
           ?>