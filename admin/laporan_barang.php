
<?php 
include "header.php";
?>

    <button type="button"  data-toggle="modal" data-target="#export" class="btn icon-print red"><i class="fa fa-print" aria-hidden="true" style="color: white;"> Cetak Ke PDF</i></a></button>
                                    <button type="button" data-toggle="modal" data-target="#excel" class="btn icon-share-alt green" aria-hidden="true" style="color: white;"> Cetak Ke Excel</i></a></button>
                                    <br><br>
                      <div class="row-fluid sortable">      
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2>Data Laporan Barang</h2>
                       
                    </div>
                    <div class="box-content">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
                            <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Kondisi</th>
                                            <th>Keterangan</th>
										    <th>Jumlah</th>
											<th>Jenis</th>
											<th>Tanggal Registrasi</th>
                                            <th>Ruang</th>
                                            <th>Kode Inventaris</th>
                                            <th>Petugas</th>
                                            <th>Sumber</th>
											
                                        </tr>
                                        </thead>


                                        <tbody>
										 <?php
                                        include '../koneksi.php';
                                        $no =1;
                                        $data = mysqli_query($koneksi,"SELECT * from inventaris INNER JOIN jenis ON inventaris.id_jenis=jenis.id_jenis INNER JOIN ruang ON inventaris.id_ruang=ruang.id_ruang INNER JOIN petugas ON inventaris.id_petugas=petugas.id_petugas  order by id_inventaris desc ");
                                        while($r = mysqli_fetch_array($data)){
                                        ?>
                                        <tr>
                                            <td><?php echo $no++;?></td>
                                            <td><?php echo $r['nama']; ?></td>
                                            <td><?php echo $r['kondisi']; ?></td>
                                            <td><?php echo $r['keterangan']; ?></td>
											<td><?php echo $r['jumlah']; ?></td>
											<td><?php echo $r['nama_jenis']; ?></td>
                                            <td><?php echo $r['tanggal_register']; ?></td>
											<td><?php echo $r['nama_ruang']; ?></td>
                                            <td><?php echo $r['kode_inventaris']; ?></td>
                                            <td><?php echo $r['nama_petugas']; ?></td>
                                            <td><?php echo $r['sumber']; ?></td>
											

                                        </tr>
                                       <?php
										}
										?>
                                        </tbody>
                                    </table>
                                   
                                    
                                </div>
                            </div>
                        </div>



<div id="export" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Data Barang</h4>
</div>
<div class="modal-body">
<form action="ctk_pdf.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Cetak">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="ctk_allpdf.php" target="_blank" class="btn btn-primary" >Cetak Semua</a>
</div>
</div>
</div>
</div>
</div>



<div id="excel" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Data Barang </h4>
</div>
<div class="modal-body">
<form action="ctk_excel.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-danger" value="Cetak">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="ctk_all_excel.php" target="_blank" class="btn btn-info btn-sm" >Cetak Semua</a>
</div>
</div>
</div>
</div>
</div>
</div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
           <?php
           include "footer.php";
           ?>