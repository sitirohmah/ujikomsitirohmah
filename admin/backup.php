
<?php
$connect = new PDO("mysql:host=localhost;dbname=inventaris_smk", "root", "");
$get_all_table_query = "SHOW TABLES";
$statement = $connect->prepare($get_all_table_query);
$statement->execute();
$result = $statement->fetchAll();

if(isset($_POST['table']))
{
 $output = '';
 foreach($_POST["table"] as $table)
 {
  $show_table_query = "SHOW CREATE TABLE " . $table . "";
  $statement = $connect->prepare($show_table_query);
  $statement->execute();
  $show_table_result = $statement->fetchAll();

  foreach($show_table_result as $show_table_row)
  {
   $output .= "\n\n" . $show_table_row["Create Table"] . ";\n\n";
  }
  $select_query = "SELECT * FROM " . $table . "";
  $statement = $connect->prepare($select_query);
  $statement->execute();
  $total_row = $statement->rowCount();

  for($count=0; $count<$total_row; $count++)
  {
   $single_result = $statement->fetch(PDO::FETCH_ASSOC);
   $table_column_array = array_keys($single_result);
   $table_value_array = array_values($single_result);
   $output .= "\nINSERT INTO $table (";
   $output .= "" . implode(", ", $table_column_array) . ") VALUES (";
   $output .= "'" . implode("','", $table_value_array) . "');\n";
  }
 }
 $file_name = 'inventaris_smk_' . date('y-m-d') . '.sql';
 $file_handle = fopen($file_name, 'w+');
 fwrite($file_handle, $output);
 fclose($file_handle);
 header('Content-Description: File Transfer');
 header('Content-Type: application/octet-stream');
 header('Content-Disposition: attachment; filename=' . basename($file_name));
 header('Content-Transfer-Encoding: binary');
 header('Expires: 0');
 header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file_name));
    ob_clean();
    flush();
    readfile($file_name);
    unlink($file_name);
}

?>
<?php
include 'header.php';
?>

                <div class="row-fluid">	
				<div class="box span12">
					<div class="box-header">
						<h2>Backup To Database</h2>
					</div>
					<div class="box-content">
						<div class="alert alert-block span5 pull-right"> 
							
							<marquee><h3 class="alert-heading">Warning!</h3></marquee>
							<p align="black">Jika Ingin MemBackup Database Per Tabel atau Semua silahkan klik tombol Export dibawah!</p>
						</div>
    <h2 align="center">Backup Database</h2>
    <br />
    <form method="post" id="export_form" >
     <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pilih Tabel Untuk Export</h3>
    <?php
    foreach($result as $table)
    {
    ?>
     <div class="checkbox">
      <label><input type="checkbox" class="checkbox_table" name="table[]" value="<?php echo $table["Tables_in_inventaris_smk"]; ?>" /> <?php echo $table["Tables_in_inventaris_smk"]; ?></label>
     </div>
    <?php
    }
    ?>
     <div class="form-group">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" id="submit" class="btn btn-danger" value="Export" />
     </div>
      <div class="box-content">
            <div class=""> 
              
            </div>
    <h2>&nbsp;&nbsp;Backup Database Semua</h2>
     <div class="form-group">
      <?php
                              error_reporting(0);
                              $file=date("Ymd").'_backup_database_'.time().'.sql';
                              backup_tables("localhost","root","","inventaris_smk",$file);
                            ?>
                                <div class="form-group pull-left">
                                  &nbsp;&nbsp;<a style="cursor:pointer" onclick="location.href='download_backup_data.php?nama_file=<?php echo $file;?>'" title="Download" class="btn btn-primary" > Export</a>
                                </div> 
                                <?php
            /*
            untuk memanggil nama fungsi :: jika anda ingin membackup semua tabel yang ada didalam database, biarkan tanda BINTANG (*) pada variabel $tables = '*'
            jika hanya tabel-table tertentu, masukan nama table dipisahkan dengan tanda KOMA (,) 
            */
            function backup_tables($host,$user,$pass,$name,$nama_file,$tables ='*') {
              $link = mysql_connect($host,$user,$pass);
              mysql_select_db($name,$link);
              
              if($tables == '*'){
                $tables = array();
                $result = mysql_query('SHOW TABLES');
                while($row = mysql_fetch_row($result)){
                  $tables[] = $row[0];
                }
              }
              else{//jika hanya table-table tertentu
                $tables = is_array($tables) ? $tables : explode(',',$tables);
              }
              
              foreach($tables as $table){
                $result = mysql_query('SELECT * FROM '.$table);
                $num_fields = mysql_num_fields($result);
                                
                $return.= 'DROP TABLE '.$table.';';//menyisipkan query drop table untuk nanti hapus table yang lama
                $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
                $return.= "\n\n".$row2[1].";\n\n";
                
                for ($i = 0; $i < $num_fields; $i++) {
                  while($row = mysql_fetch_row($result)){
                    //menyisipkan query Insert. untuk nanti memasukan data yang lama ketable yang baru dibuat
                    $return.= 'INSERT INTO '.$table.' VALUES(';
                    for($j=0; $j<$num_fields; $j++) {
                      //akan menelusuri setiap baris query didalam
                      $row[$j] = addslashes($row[$j]);
                      $row[$j] = ereg_replace("\n","\\n",$row[$j]);
                      if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                      if ($j<($num_fields-1)) { $return.= ','; }
                    }
                    $return.= ");\n";
                  }
                }
                $return.="\n\n\n";
              }             
              //simpan file di folder
              $nama_file;
              
              $handle = fopen('backup/'.$nama_file,'w+');
              fwrite($handle,$return);
              fclose($handle);
            }
            ?>
    </form>
   </div>
  </div>
 </body>
</html>
<script>
$(document).ready(function(){
 $('#submit').click(function(){
  var count = 0;
  $('.checkbox_table').each(function(){
   if($(this).is(':checked'))
   {
    count = count + 1;
   }
  });
  if(count > 0)
  {
   $('#export_form').submit();
  }
  else
  {
   alert("Please Select Atleast one table for Export");
   return false;
  }
 });
});
</script>

</div>
</div>
</div>
</div>
</div>
</div>

		
	<?php
	include 'footer.php';
	?>