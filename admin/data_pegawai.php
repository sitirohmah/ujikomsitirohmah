<?php
include 'header.php';
?>
	
 <div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-span6 ">
                    <div class="box-header" data-original-title>
                        <h2>Form Input Pegawai</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
                </div>
                    <div class="modal-body">
                        <form class="form-horizontal" action="simpan_pegawai.php" method="post" enctype="multipart/form-data">  
                              
                              <div class="control-group" >
                                <label class="control-label" >Nama Pegawai :</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off" id="nama_pegawai" name="nama_pegawai" required="" >
                                </div>
                              </div>

                               <div class="control-group" >
                                <label class="control-label" for="">NIP :</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off" id="nip" name="nip" required="" >
                                </div>
                              </div>

                              <div class="control-group" >
                                <label class="control-label" for="">Alamat :</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off"  name="alamat" required="" >
                                </div>
                              </div>
                            
                            <br>

                            <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                            </form>
                            </div>
                            </div>
                            </div>
                            </div>
        </div>
    </div>
                            

<a href="#" data-toggle="modal" data-target="#myModal"><i class="btn icon-plus red"> Tambah Data </a></i>
<br><br>

		<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>Data Pegawai</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
                                         <tr>
                                        <th>No</th>
                                        <th>Nama Pegawai</th>
                                        <th>NIP</th>
                                        <th>Alamat</th>
                                        <th>Aksi</th>
                                        </tr> 
                                    </thead>

                                 <tbody>
                                  
        
 <?php
include "../koneksi.php";
$no=1;
$bacadata = mysqli_query($koneksi,"select * from pegawai ORDER BY id_pegawai desc");
while($data = mysqli_fetch_array($bacadata))
{
  ?>

                                      <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nama_pegawai']; ?></td>
                                        <td><?php echo $data['nip']; ?></td>
                                        <td><?php echo $data['alamat']; ?></td>
                                       
                                        
                                            <td><a class="btn icon-edit red" href="edit_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>">Edit</a>  
                                            <a class="btn icon-trash blue" href="hapus_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>">Hapus</a></td> 


                                        </tr>

                                        <?php
                                      }
                                    ?>

                    
                                    </tbody>
                                </table>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>


		
	<?php
	include 'footer.php';
	?>