<?php
session_start();
include '../koneksi.php';
if(!isset($_SESSION['username'])) {
	  echo "<script type=text/javascript>
    alert('Anda Belum Login!!');
    window.location='../index.php';</script>";
	
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Inventaris Sarana & Prasarana SMKN 1 Ciomas</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial- scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="../css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="../css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>

	
	
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="../img/favicon.ico">
	<!-- end: Favicon -->
	
		
		
		
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.php"><span>Invensarpras</span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<div class="navbar-inner">
					<ul class="nav pull-right">
						<!-- start: User Dropdown -->
						<?php 
							$use=$_SESSION['username'];
							$fo=mysqli_query($koneksi,"select nama_petugas from petugas where username='$use'");
								while($f=mysqli_fetch_array($fo)){
									?>
						<li class="dropdown"> 
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> Hii, <?php echo $f['nama_petugas'];?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
 									<span>Account Settings</span>
								</li>
								
								<li><a href="../logout.php"><i class="halflings-icon share"></i> Logout</a></li>
							</ul>
						</li>		
						<?php
						 }
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
							
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li><a href="dashboard.php"><i class="icon-dashboard"></i><span class="hidden-tablet"> Dashboard</span></a></li>	
						<li>
							<a class="dropmenu" href="#"><i class="icon-list-alt"></i><span class="hidden-tablet"> Inventarisir</span><span class="label label-important"></span></a>
							<ul>
								<li><a class="submenu" href="data_inventaris.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Data Barang</span></a></li>
								<li><a class="submenu" href="data_jenis.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Data Jenis</span></a></li>
								<li><a class="submenu" href="data_ruang.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Data Ruang</span></a></li>
								
							</ul>	
						</li>
						
						<li>
							<a class="dropmenu" href="#"><i class="icon-retweet"></i><span class="hidden-tablet"> Transaksi</span><span class="label label-important"></span></a>
							<ul>
								
								<li><a class="submenu" href="peminjaman.php"><i class="icon-upload"></i><span class="hidden-tablet"> Peminjaman</span></a></li>
								<li><a class="submenu" href="pengembalian.php"><i class="icon-download"></i><span class="hidden-tablet"> Pengembalian</span></a></li>
								
							</ul>	
						</li>
						<li>
							<a class="dropmenu" href="#"><i class="icon-user"></i><span class="hidden-tablet"> Users</span><span class="label label-important"></span></a>
							<ul>
								
								<li><a class="submenu" href="data_petugas.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Data Petugas</span></a></li>
								<li><a class="submenu" href="data_pegawai.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Data Pegawai</span></a></li>
								
							</ul>	
						</li>

						<li>
							<a class="dropmenu" href="#"><i class="icon-file-alt"></i><span class="hidden-tablet"> Laporan</span><span class="label label-important"></span></a>
							<ul>
								<li><a class="submenu" href="laporan_barang.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Laporan Barang</span></a></li>
								<li><a class="submenu" href="laporan_peminjaman.php"><i class="icon-file-alt"></i><span class="hidden-tablet"> Laporan Peminjaman</span></a></li>

							</ul>	
							<li><a href="backup.php"><i class="icon-cog"></i><span class="hidden-tablet"> Backup</span></a></li>	
						</li>
					</ul>
				</div>
			</div>
		
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="data_inventaris.php">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="dashboard.php">Dashboard</a></li>
			</ul>
			<div class="row-fluid">	
			</div>