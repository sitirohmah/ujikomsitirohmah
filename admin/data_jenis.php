<?php
include 'header.php';
?>
			
	 <div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-span6 ">
                    <div class="box-header" data-original-title>
                        <h2>Form Input Jenis Barang</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
                </div>
                    <div class="modal-body">
                        <form class="form-horizontal" action="simpan_jenis.php" method="post" enctype="multipart/form-data">  
                              
                              <div class="control-group" >
                                <label class="control-label" >Nama Jenis :</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off" id="nama_jenis" name="nama_jenis" required="harus diisi" >
                                </div>
                              </div>

                               <div class="control-group" >
                                <label class="control-label" for="">Kode Jenis :</label>
                                <div class="controls">
                                    <?php
                                    include "../koneksi.php";
                           $koneksi = mysqli_connect("localhost","root","","inventaris_smk");
                           
                           $cari_kd=mysqli_query($koneksi,"select max(kode_jenis) as kode from jenis");
                           //besar atau kode yang baru masuk
                           $tm_cari=mysqli_fetch_array($cari_kd);
                           $kode=substr($tm_cari['kode'],1,4);
                           $tambah=$kode+1;
                           if($tambah<10){
                            $kode_jenis="J000".$tambah;
                           }else{ 
                            $kode_jenis="J00".$tambah;
                           }
                           ?>
                                  <input type="text" class="span8 typeahead" autocomplete="off" id="kode_jenis" name="kode_jenis" required="harus diisi"  value="<?php echo $kode_jenis; ?>" readonly >
                                </div>
                              </div>

                              <div class="control-group" >
                                <label class="control-label" for="">Keterangan :</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off"  name="keterangan" required="harus diisi" >
                                </div>
                              </div>

                              
                            
                            <br>

                            <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                            </form>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>


<a href="#" data-toggle="modal" data-target="#myModal"><i class="btn icon-plus red"> Tambah Data </a></i>
<br><br>

		<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>Data Jenis Barang</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
                                         <tr>
                                        <th>No</th>
                                        <th>Nama Jenis</th>
                                        <th>Kode Jenis</th>
                                        <th>Keterangan</th>
                                        <th>Aksi</th>
                                        </tr> 
                                    </thead>

                                 <tbody>
                                  
        
 <?php
include "../koneksi.php";
$no=1;
$bacadata = mysqli_query($koneksi,"select * from jenis order by id_jenis desc");
while($data = mysqli_fetch_array($bacadata))
{
  ?>

                                      <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nama_jenis']; ?></td>
                                        <td><?php echo $data['kode_jenis']; ?></td>
                                        <td><?php echo $data['keterangan']; ?></td>
                                        
                                       
                                        
                                            <td><a class="btn icon-edit red" href="edit_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>">Edit</a>  
                                            <a class="btn icon-trash blue" href="hapus_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>">Hapus</a></td> 


                                        </tr>

                                        <?php
                                      }
                                    ?>

                    
                                    </tbody>
                                </table>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>


		
	<?php
	include 'footer.php';
	?>