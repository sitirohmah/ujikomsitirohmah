<?php
include 'header.php';
?>
	 <div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-span6 ">
                    <div class="box-header" data-original-title>
                        <h2>Form Input Petugas</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
                </div>
                    <div class="modal-body">
                        <form class="form-horizontal" action="simpan_petugas.php" method="post" enctype="multipart/form-data">  
                              
                              <div class="control-group" >
                                <label class="control-label" >Username :</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off" id="username" name="username" required="harus diisi" >
                                </div>
                              </div>

                               <div class="control-group" >
                                <label class="control-label" for="">Email :</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off" id="email" name="email" required="harus diisi" >
                                </div>
                              </div>

                              <div class="control-group" >
                                <label class="control-label" for="">Password :</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off"  name="password" required="harus diisi" >
                                </div>
                              </div>

                               <div class="control-group" >
                                <label class="control-label" for="">Nama Petugas :</label>
                                <div class="controls">
                                  <input type="text" class="span8 typeahead" autocomplete="off" id="nama_petugas" name="nama_petugas" required="harus diisi" >
                                </div>
                              </div>

                              
                               <div class="control-group">
                              <?php
                              include "../koneksi.php";
                              $result = mysqli_query($koneksi,"select * from level order by id_level asc ");
                              $jsArray = "var id_level = new Array();\n";
                               ?> 
                                <label class="control-label" for="">Level :</label>
                                <div class="controls">
                                  <select class="span8 typeahead"  name="id_level" onchange="changeValue(this.value)" required="">
                                  <option selected="selected">..........Pilih Level..........
  <?php 
  while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
    $jsArray .= "id_level['". $row['id_level']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
  }
  ?>
                             </option>
                             </select> 
                            </div>
                            </div> 
                            
                             <div class="control-group">
                                <label class="control-label">Banned :</label>
                                <div class="controls">
                                  <label class="checkbox inline">
                                    <input type="checkbox" name ="banned" value="N"> N
                                  </label>
                                  <label class="checkbox inline">
                                    <input type="checkbox" name="banned" value="Y"> Y
                                  </label>
                                </div>
                              </div>

                               <div class="control-group" >
                                <label class="control-label" for="">Login Time :</label>
                                <div class="controls">
                                  <input type="number" class="span8 typeahead" autocomplete="off" id="logintime" name="logintime" required="harus diisi" >
                                </div>
                              </div>           
                            
                            <br>

                            <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                            </form>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                           
    
<a href="#" data-toggle="modal" data-target="#myModal"><i class="btn icon-plus red"> Tambah Data </a></i>
<br><br>

		<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2>Data Petugas</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
                                         <tr>
                                        <th>No</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Password</th>
                                        <th>Nama Petugas</th>
                                        <th> Level</th>
                                        <th>Banned</th>
                                        <th>Login Time</th>
                                        <th>Aksi</th>
                                        </tr> 
                                    </thead>

                                 <tbody>
                                  
        
 <?php
include "../koneksi.php";
$no=1;
$bacadata = mysqli_query($koneksi,"select * from petugas INNER JOIN level ON petugas.id_level=level.id_level ORDER BY id_petugas desc");
while($data = mysqli_fetch_array($bacadata))
{
  ?>

                                      <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['username']; ?></td>
                                        <td><?php echo $data['email']; ?></td>
                                        <td><?php echo $data['password']; ?></td>
                                        <td><?php echo $data['nama_petugas']; ?></td>
                                        <td><?php echo $data['nama_level']; ?></td>
                                        <td><?php echo $data['banned']; ?></td>
                                        <td><?php echo $data['logintime']; ?></td>
                                       
                                        
                                            <td><a class="btn icon-edit red" href="edit_petugas.php?id_petugas=<?php echo $data['id_petugas']; ?>">Edit</a>  
                                            <a class="btn icon-trash blue" href="hapus_petugas.php?id_petugas=<?php echo $data['id_petugas']; ?>">Hapus</a></td> 


                                        </tr>

                                        <?php
                                      }
                                    ?>

                    
                                    </tbody>
                                </table>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>


		
	<?php
include 'footer.php';
?>