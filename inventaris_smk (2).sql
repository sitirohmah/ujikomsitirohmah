-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02 Apr 2019 pada 07.03
-- Versi Server: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `inventaris_smk`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

CREATE TABLE IF NOT EXISTS `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `jumlah_pinjam` int(11) NOT NULL,
  `id_pinjam` int(11) NOT NULL,
  `status` enum('dipinjam','kembalikan','','') NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_inventaris` (`id_inventaris`),
  KEY `id_peminjaman` (`id_peminjaman`,`id_pinjam`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data untuk tabel `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `id_inventaris`, `id_peminjaman`, `jumlah_pinjam`, `id_pinjam`, `status`) VALUES
(12, 1, 1, 24, 3, 'dipinjam');

--
-- Trigger `detail_pinjam`
--
DROP TRIGGER IF EXISTS `kembali`;
DELIMITER //
CREATE TRIGGER `kembali` BEFORE UPDATE ON `detail_pinjam`
 FOR EACH ROW UPDATE inventaris SET jumlsh = jumlah+NEW.jumlah WHERE id_inventaris = new.id_inventaris
//
DELIMITER ;
DROP TRIGGER IF EXISTS `pengurangan_stok`;
DELIMITER //
CREATE TRIGGER `pengurangan_stok` AFTER INSERT ON `detail_pinjam`
 FOR EACH ROW UPDATE inventaris SET jumlah = jumlah -NEW.jumlah WHERE id_inventaris = new.id_inventaris
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris`
--

CREATE TABLE IF NOT EXISTS `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `spesifikasi` text NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(20) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `sumber` text NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `spesifikasi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`, `sumber`) VALUES
(1, 'Laptop', 'baik', 'Intel ', 'ada di Lab 2', 4, 1, '2019-02-12', 1, 'V0001', 1, 'PT Maju Prima'),
(2, 'Kursi', 'baik', 'warna coklat', 'aaaaa', 32, 2, '2019-02-20', 2, 'V0002', 2, 'dari PT'),
(3, 'Meja', 'baik', 'warna coklat', 'ada digudang', 9, 1, '2019-02-27', 3, 'V0003', 3, 'Pt.Suka Maju');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE IF NOT EXISTS `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(30) NOT NULL,
  `kode_jenis` varchar(30) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(1, 'Elektronik', 'J0001', 'Di Lab 1'),
(2, 'Non Elektronik', 'J0002', 'Di Gudang'),
(6, 'Mebel', 'J0003', 'Di Lantai Bawah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(30) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'operator'),
(3, 'peminjam');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(30) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `alamat` varchar(40) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`) VALUES
(2, 'lala', '1213dfe', 'bogor'),
(3, 'Fahmi', '000123', 'davdav');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE IF NOT EXISTS `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` varchar(30) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_pegawai` (`id_pegawai`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_pegawai`) VALUES
(1, '2019-01-09', '2019-01-14', 'dipinjam', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE IF NOT EXISTS `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(30) NOT NULL,
  `nama_petugas` varchar(30) NOT NULL,
  `id_level` int(11) NOT NULL,
  `banned` enum('N','Y','','') NOT NULL,
  `logintime` int(11) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `email`, `password`, `nama_petugas`, `id_level`, `banned`, `logintime`) VALUES
(1, 'operator', '', 'operator4321', 'rahma', 2, 'N', 0),
(2, 'admin', 'strhmh811@gmail.com', 'admin4321', 'siti rahma', 1, 'N', 0),
(3, 'peminjam', '', 'peminjam4321', 'Danu Ilham', 3, 'N', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE IF NOT EXISTS `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(30) NOT NULL,
  `kode_ruang` varchar(30) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(1, 'Di Lab 1', 'R0001', 'Baik'),
(2, 'Di Lab 2', 'R0002', 'Baik'),
(3, 'Di Gudang', 'R0003', 'Baik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `smtr_detail`
--

CREATE TABLE IF NOT EXISTS `smtr_detail` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `smtr_detail`
--

INSERT INTO `smtr_detail` (`id_detail_pinjam`, `id_inventaris`, `jumlah_pinjam`) VALUES
(1, 1, 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
